﻿DROP DATABASE IF EXISTS ejemplo001;
CREATE DATABASE ejemplo001;
USE ejemplo001;

CREATE TABLE cursos(
  -- crear campos
  idCurso int,
  nombre varchar(100) NOT NULL,
  fechaComienzo date,
  precio float DEFAULT 0,
  -- claves
  PRIMARY KEY(idCurso),
  CONSTRAINT uk1 UNIQUE KEY (nombre),
  KEY clave1 (fechaComienzo),
  KEY k1 (fechaComienzo,precio)
);