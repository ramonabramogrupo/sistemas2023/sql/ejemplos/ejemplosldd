﻿/*
  ejemplo de clase inicial
*/

DROP DATABASE IF EXISTS ejemplo001;

CREATE DATABASE ejemplo001;

USE ejemplo001;

CREATE TABLE informes (
  id int AUTO_INCREMENT,
  fecha datetime NOT NULL,
  observaciones text,
  PRIMARY KEY (id)
);

-- listar informes con fecha 3/1/2024  
SELECT * FROM informes i WHERE i.fecha='2024-01-03'; 



