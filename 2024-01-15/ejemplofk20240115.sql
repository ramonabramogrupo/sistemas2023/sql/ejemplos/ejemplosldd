﻿DROP DATABASE IF EXISTS ejemploFK; -- ELIMINA BASE DE DATOS

CREATE DATABASE ejemploFK;

USE ejemploFK; #SELECCIONA BASE DE DATOS

-- necesito primero crear la tabla principal
CREATE TABLE ciudades (
  #Definicion de campos
  ciudad_id int AUTO_INCREMENT,
  ciudad_nom varchar(50) NULL DEFAULT NULL,
  `provincia` varchar(50) NOT NULL DEFAULT 'Cantabria',
  #Definicion de indices
  PRIMARY KEY(ciudad_id),
  INDEX indice1(ciudad_nom,provincia)
);


-- realizo la tabla relacionada
CREATE TABLE personas (
  #Definicion de campos
  persona_id int AUTO_INCREMENT,
  persona_nom varchar(50) NOT NULL,
  ciudad_id int DEFAULT 2,
  correo varchar(50),

  # Definicion de indices
  PRIMARY KEY(persona_id), -- clave principal
  INDEX indice1(persona_nom), -- indexado con duplicados
  UNIQUE uk1(correo), -- indexado sin duplicados
  # claves ajenas
  -- FOREIGN KEY (ciudad_id) REFERENCES ciudades (ciudad_id)
  -- FOREIGN KEY fkPersonasCiudades (ciudad_id) REFERENCES ciudades (ciudad_id)
  -- CONSTRAINT fkPersonasCiudades FOREIGN KEY (ciudad_id) REFERENCES ciudades (ciudad_id)

  # OPCION DE CASCADA
  -- CONSTRAINT fkPersonasCiudades 
    -- FOREIGN KEY (ciudad_id) REFERENCES ciudades (ciudad_id)
    -- SI ELIMINO UNA CIUDAD Y HAY PERSONAS EN ESA CIUDAD SE ELIMINAN LAS PERSONAS
    -- ON DELETE CASCADE 
    -- SI ACTUALIZO EL CAMPO CLAVE PRINCIPAL DE UNA CIUDAD SE ACTUALIZA ESE CIUDAD_ID 
    -- EN LAS PERSONAS QUE VIVEN EN ESA CIUDAD
    -- ON UPDATE CASCADE 

  # OPCION DE RESTRICT
   -- CONSTRAINT fkPersonasCiudades 
    -- FOREIGN KEY (ciudad_id) REFERENCES ciudades (ciudad_id)
    -- SI ELIMINO UNA CIUDAD Y HAY PERSONAS EN ESA CIUDAD 
    -- NO PUEDO ELIMINAR LA CIUDAD
    -- ON DELETE RESTRICT
    -- SI ACTUALIZO EL CAMPO CLAVE PRINCIPAL DE UNA CIUDAD Y HAY PERSONAS QUE VIVAN EN ESA CIUDAD 
    -- NO PUEDO ACTUALIZAR
    -- ON UPDATE RESTRICT 

  # OPCION DE SET NULL
   -- CONSTRAINT fkPersonasCiudades 
    -- FOREIGN KEY (ciudad_id) REFERENCES ciudades (ciudad_id)
    -- SI ELIMINO UNA CIUDAD Y HAY PERSONAS EN ESA CIUDAD 
    -- ME COLOCA A NULL EL CODIGO POSTAL DE ESAS PERSONAS
    -- ON DELETE SET NULL
    -- SI ACTUALIZO EL CAMPO CLAVE PRINCIPAL DE UNA CIUDAD Y HAY PERSONAS QUE VIVAN EN ESA CIUDAD 
    -- ME COLOCA A NULL EL CODIGO POSTAL DE ESAS PERSONAS
    -- ON UPDATE set NULL 

# OPCION DE SET DEFAULT
   CONSTRAINT fkPersonasCiudades 
    FOREIGN KEY (ciudad_id) REFERENCES ciudades (ciudad_id)
    -- SI ELIMINO UNA CIUDAD Y HAY PERSONAS EN ESA CIUDAD 
    -- ME COLOCA AL VALOR POR DEFECTO EL CAMPO CIUDAD_ID DE ESA PERSONAS
    ON DELETE SET DEFAULT
    -- SI ACTUALIZO EL CAMPO CLAVE PRINCIPAL DE UNA CIUDAD Y HAY PERSONAS QUE VIVAN EN ESA CIUDAD 
    -- ME COLOCA AL VALOR POR DEFECTO EL CAMPO CIUDAD_ID DE ESAS PERSONAS
    ON UPDATE SET DEFAULT
 
);



/*
  Introducir datos de pruebas
*/

-- primero tengo que meter las ciudades para poder meter las personas
INSERT ciudades (ciudad_nom, provincia)
  VALUES
  ('santander', 'cantabria'),
  ('Potes','cantabria'),
  ('Laredo',DEFAULT),
  ('Reinosa',DEFAULT);

INSERT ciudades (ciudad_nom)
  VALUES ('Torrelavega'); -- al no colocar nada en provincia te coloca CANTABRIA

-- ahora puedo meter personas siempre que la ciudad exista
INSERT INTO personas (persona_id, persona_nom, ciudad_id)
  VALUES  
  (1, 'Jose', 1),
  (2, 'Ana', 1),
  (3,'silvia',2);





