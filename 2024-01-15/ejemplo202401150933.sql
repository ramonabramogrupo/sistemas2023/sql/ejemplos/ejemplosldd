﻿-- crear base de datos y seleccionarla
DROP DATABASE IF EXISTS ejemplo001;
CREATE DATABASE ejemplo001;
USE ejemplo001;


-- CREAR LA TABLA COCHES

CREATE TABLE coches(
  # campos
  codigo int NOT NULL,
  matricula char(7) NOT NULL,
  marca varchar(100),
  modelo varchar(100),
  fecha date NOT NULL DEFAULT '2024-1-1',
  
  # indices
  # clave principal
  PRIMARY KEY(codigo),

  # indexados sin duplicados
  CONSTRAINT ukMatricula UNIQUE KEY (matricula),
-- UNIQUE (matricula),

  # indexado con duplicados
  INDEX kmarcamodelo (marca,modelo),
  INDEX kfecha (fecha),

  # reglas
  CONSTRAINT checkFecha CHECK(fecha<'2025-1-1'),
  -- CHECK (fecha<'2025-1-1'),

  CONSTRAINT checkCodigo CHECK(codigo>0)
     
);


-- meter unos datos de prueba

INSERT INTO coches 
  (codigo, matricula, marca, modelo, fecha) VALUES 
  (1,'3456dgh','ford','mondeo','2024-1-15');

-- compruebo que entra el registro
SELECT * FROM coches;

-- intentar saltarnos la clave principal

-- error ya que codigo es clave principal
-- INSERT INTO coches 
--   ( matricula, marca, modelo, fecha) VALUES 
--   ('3456dgg','ford','mondeo','2024-1-15');

-- error ya que el codigo ya existe
-- INSERT INTO coches 
--   (codigo, matricula, marca, modelo, fecha) VALUES 
--   (1,'3456dgg','ford','mondeo','2024-1-15');

-- probamos el indexado sin duplicados

-- INSERT INTO coches 
--   (codigo, matricula, marca, modelo, fecha) VALUES 
--   (2,'3456dgh','ford','mondeo','2024-1-15');

-- probar requeridos

-- el registro entra porque la fecha tiene un valor por defecto
INSERT INTO coches 
  (codigo, matricula, marca, modelo) VALUES 
  (2,'3456fgh','ford','mondeo');

-- el registro no entra porque la fecha es requerida
-- INSERT INTO coches 
--   (codigo, matricula, marca, modelo,fecha) VALUES 
--   (3,'3466fgh','ford','mondeo',NULL);

-- probamos los checks

-- introducimos una fecha incorrecta
-- INSERT INTO coches 
--   (codigo, matricula, marca, modelo,fecha) VALUES 
--   (3,'4456fgh','ford','mondeo','2026-1-1');

-- introducimos un codigo incorrecto
-- INSERT INTO coches 
--   (codigo, matricula, marca, modelo,fecha) VALUES 
--   (-1,'4456fgh','ford','mondeo','2023-1-1');


-- SELECT * FROM coches c;