﻿DROP DATABASE IF EXISTS reglas;

CREATE DATABASE reglas;

USE reglas;

CREATE TABLE practicas(
  # campos
  id int AUTO_INCREMENT,
  nota float,

  #claves
  PRIMARY KEY (id),

  #reglas
  
  # la nota debe ser mayor que 0
  -- CHECK (nota>0), -- correcto pero el nombre se lo asigna el servidor
  CONSTRAINT checkNota CHECK (nota>0)


);


-- insercion multiple
INSERT IGNORE INTO practicas 
  (nota) VALUES 
  (8),
  (0),
  (7);

-- insercion simple
INSERT IGNORE INTO practicas 
  (nota) VALUES 
  (8);

INSERT IGNORE INTO practicas 
  (nota) VALUES 
  (0);
  
INSERT IGNORE INTO practicas 
  (nota) VALUES 
  (7);


SELECT * FROM practicas p;