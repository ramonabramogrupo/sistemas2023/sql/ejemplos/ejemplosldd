﻿-- eliminar la base de datos alumnos si existe

DROP DATABASE IF EXISTS alumnos;

-- crear la base de datos alumnos

CREATE DATABASE alumnos;

-- seleccionar la base de datos

USE alumnos;

-- crear la tabla alumnos

create TABLE alumnos(
  # campos
  id int AUTO_INCREMENT,
  nombre varchar(100),
  apellidos varchar(200),
  telefono varchar(20) NOT NULL,
  fechaNacimiento date,
  email varchar(100) NOT NULL,
  poblacion varchar(100) DEFAULT 'Santander',
  # indices
  
  # clave principal
  PRIMARY KEY(id),
  
  # indexados sin duplicados
  CONSTRAINT uk1 UNIQUE KEY (nombre,apellidos),
  -- UNIQUE uk1 (nombre,apellidos),
  CONSTRAINT email UNIQUE KEY (email),
  -- UNIQUE (email),
  
  # indexados con duplicados
  KEY (telefono),
  -- KEY telefono (telefono),
  -- INDEX (telefono),

  INDEX (poblacion)
  -- INDEX poblacion (poblacion)

);


-- crear la tabla cursos
CREATE TABLE cursos(
  # Campos
  idCurso int AUTO_INCREMENT,
  nombre varchar(100) NOT NULL,
  fechaComienzo date,
  precio float DEFAULT 0,

  # Claves

  # Clave principal
  PRIMARY KEY (idCurso),

  # indexados sin duplicados

  UNIQUE (nombre),
  -- UNIQUE nombre (nombre),
  -- CONSTRAINT nombre UNIQUE (nombre),

  # indexado con duplicados

  INDEX (fechaComienzo),
  -- INDEX fechaComienzo (fechaComienzo),

  INDEX k1 (fechaComienzo,precio),
  -- KEY k1 (fechaComienzo,precio)

  # checks (reglas de validacion a nivel de campo)
  -- CHECK (precio>=0),
  CONSTRAINT check1 CHECK (precio>=0)
   
);

