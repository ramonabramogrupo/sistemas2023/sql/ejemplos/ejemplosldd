﻿CREATE TABLE codigosPostales(
codigoPostal char(5),
provincia varchar(100),
poblacion varchar(100),
PRIMARY KEY(codigoPostal)
);

CREATE TABLE clientes(
codigo int,
nombre varchar(100),
cp char(5),

# claves
PRIMARY KEY(codigo),
# clave ajena
CONSTRAINT fkClientesCodigos FOREIGN KEY (cp) REFERENCES codigosPostales (codigoPostal) ON DELETE SET DEFAULT ON UPDATE SET DEFAULT
);