﻿DROP DATABASE IF EXISTS practica4;
CREATE DATABASE practica4;
USE practica4;

# deshabilitar las claves ajenas
# instruccion de control 
SET FOREIGN_KEY_CHECKS=0;


CREATE TABLE empleado(
  # campos
  nssEmpleado varchar(10),
  nombre varchar(100),
  apellido varchar(200),
  iniciales varchar(4),
  fechaNcto date,
  sexo varchar(20),
  direccion varchar(400),
  salario float,
  nombreDPertenece varchar(100),
  numeroDPertenece int,
  
  # clave principal
  PRIMARY KEY(nssEmpleado),
  
  # clave ajena
  CONSTRAINT fkEmpleadoDepartamento FOREIGN KEY (nombreDPertenece,numeroDPertenece) 
    REFERENCES departamento (nombreD,numeroD) 
    ON DELETE RESTRICT ON UPDATE RESTRICT
);

-- HABILITAR LAS CLAVES AJENAS
SET FOREIGN_KEY_CHECKS=1; 

CREATE TABLE departamento(
  # campos
  nombreD varchar(100),
  numeroD int,
  numDeEmpleados int,
  nssEmpleadoDirige varchar(10),
  fechaInicioJefe date,

  #clave principal
  PRIMARY KEY(nombreD,numeroD),

  #indexados sin duplicados
  CONSTRAINT uk1 UNIQUE KEY(nssEmpleadoDirige),

  # claves ajenas
  CONSTRAINT fkDepartamentoEmpleado 
    FOREIGN KEY (nssEmpleadoDirige) REFERENCES empleado(nssEmpleado)
    ON DELETE RESTRICT ON UPDATE RESTRICT
);

CREATE TABLE supervisa(
  nssEmpleado varchar(10),
  nssSupervisor varchar(10),

  # clave principal
  PRIMARY KEY(nssEmpleado,nssSupervisor),

  #indexado sin duplicados
  CONSTRAINT uk1 UNIQUE KEY (nssEmpleado),

  # claves ajenas
  CONSTRAINT fkSupervisaEmpleado 
    FOREIGN KEY (nssEmpleado) REFERENCES empleado (nssEmpleado)
    ON DELETE RESTRICT ON UPDATE RESTRICT,

  CONSTRAINT fkSupervisaEmpleadoSupervisa 
    FOREIGN KEY (nssSupervisor) REFERENCES empleado (nssEmpleado)
    ON DELETE RESTRICT ON UPDATE RESTRICT

);

CREATE TABLE dependiente(
  
  # campos
  nombreDependiente varchar(100),
  nssEmpleado varchar(10),
  sexo varchar(20),
  fechaNcto date,
  parentesco varchar(100),
  
  # clave principal
  PRIMARY KEY(nombreDependiente,nssEmpleado),
  
  # claves ajenas
  CONSTRAINT fkDependienteEmpleado FOREIGN KEY(nssEmpleado) REFERENCES empleado(nssEmpleado)
    on DELETE RESTRICT ON UPDATE RESTRICT

);

CREATE TABLE localizaciones(
#campos
nombreD varchar(100),
numeroD int,
localizacionDept varchar(100),

# clave principal
PRIMARY KEY(nombreD,numeroD,localizacionDept),

#clave ajena
CONSTRAINT fkLocalizacionesDepartamento FOREIGN KEY (nombreD,numeroD) REFERENCES departamento(nombreD,numeroD)
  ON DELETE RESTRICT ON UPDATE RESTRICT                                              
);

CREATE TABLE proyecto(
  # campos
  numeroP int,
  nombreP varchar(100),
  localizacion varchar(200),
  nombreDControla varchar(100),
  numeroDControla int,

  #clave principal
  PRIMARY KEY (numeroP,nombreP),

  #clave ajena
  CONSTRAINT fkProyectoDepartamento FOREIGN KEY (nombreDControla,numeroDControla) 
    REFERENCES departamento(nombreD,numeroD) ON DELETE RESTRICT ON UPDATE RESTRICT

);

CREATE TABLE trabajeEn(
#campos
nssEmpleado varchar(10),
nombreP varchar(100),
numeroP int,
horas int,

#clave principal
PRIMARY KEY(nssEmpleado,nombreP,numeroP),

#claves ajenas
CONSTRAINT fkTrabajaEnEmpleado FOREIGN KEY (nssEmpleado) REFERENCES empleado (nssEmpleado)
  ON DELETE RESTRICT ON UPDATE RESTRICT,

CONSTRAINT fkTrabajaEnProyecto FOREIGN KEY (numeroP,nombreP) REFERENCES proyecto(numeroP,nombreP)
  ON DELETE RESTRICT ON UPDATE RESTRICT

);






