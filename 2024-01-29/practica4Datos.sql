﻿USE practica4;

-- deshabilitar temporalmente las claves ajenas

SET FOREIGN_KEY_CHECKS=0;

-- introducir empleados
INSERT INTO empleado 
  (nssEmpleado, nombre, nombreDPertenece, numeroDPertenece) VALUES 
  (1,'rosa','comercial',1),
  (2,'eva','comercial',2),
  (3,'jose','tecnico',1),
  (4,'luis','tecnico',1),
  (5,'cesar','tecnico',1),
  (6,'cesar','tecnico',1),
  (7,'eva','tecnico',1),
  (8,'susana','tecnico',1);
  
-- HABILITAR CLAVES AJENAS

set FOREIGN_KEY_CHECKS=1;

-- introducir departamentos

INSERT INTO departamento 
(nombreD, numeroD, nssEmpleadoDirige) VALUES 
('programacion',1,1),
('programacion',2,2),
('tecnico',1,3),
('tecnico',2,8),
('tecnico',3,4),
('comercial',1,5),
('comercial',2,6);

-- introducir supervisa

INSERT INTO supervisa 
  (nssEmpleado, nssSupervisor) VALUES 
  (1,2),
  (3,2),
  (2,4);

-- insertar dependientes

INSERT INTO dependiente 
  (nombreDependiente, nssEmpleado, parentesco) VALUES 
  ('jorge',1,'hijo'),
  ('loreto',2,'hija');


-- insertar localizaciones

INSERT INTO localizaciones 
(nombreD, numeroD, localizacionDept) VALUES 
('programacion',1,'santander'),
('programacion',2,'santander'),
('tecnico',1,'laredo'),
('tecnico',2,'laredo'),
('tecnico',3,'torrelavega'),
('comercial',1,'santander'),
('comercial',2,'loredo');

-- insertamos en proyectos
INSERT INTO proyecto 
(numeroP, nombreP, localizacion, nombreDControla, numeroDControla) VALUES 
(1,'proyecto','santander','programacion',1),
(2,'proyecto2','isla','programacion',1),
(3,'proyecto3','noja','tecnico',1);

-- insertamos registros en trabaja_en
INSERT INTO trabajeen 
(nssEmpleado, nombreP, numeroP, horas) VALUES 
(1,'proyecto',1,3),
(2,'proyecto2',2,4),
(2,'proyecto3',3,5);





  







