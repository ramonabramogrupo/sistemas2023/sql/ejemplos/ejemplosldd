﻿-- seleccionando la base de datos con la 
-- que voy a trabajar
USE ejemplo001;

DROP TABLE IF EXISTS practicas;
CREATE TABLE practicas (
  id int AUTO_INCREMENT,
  titulo varchar(100) NOT NULL  DEFAULT 'practica 1',
  fecha date DEFAULT '2024-1-1',
  peso float DEFAULT 10.00,
  numeroAlumnos int DEFAULT 0,
  categoria char(1) NOT NULL,
  PRIMARY KEY (id),
  INDEX (titulo),

  UNIQUE KEY(fecha)
  -- UNIQUE (fecha)
  -- CONSTRAINT uk1 UNIQUE (fecha)
  -- UNIQUE uk1 (fecha)
);

-- insertamos registros para probar
INSERT INTO practicas 
(categoria) VALUES 
('A');
-- comprobar si los datos estan en la tabla
SELECT * FROM practicas p;



