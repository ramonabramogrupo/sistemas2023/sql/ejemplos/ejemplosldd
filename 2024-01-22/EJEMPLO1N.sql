﻿-- gestion de la base de datos
DROP DATABASE IF EXISTS practicas;
CREATE DATABASE practicas;
USE practicas;

-- TABLA ALUMNOS

CREATE TABLE alumno(
  matricula int,
  nombre varchar(100),
  grupo varchar(100),
  PRIMARY KEY(matricula)
);

CREATE TABLE practica(
  codigo int,
  titulo varchar(100),
  dificultad varchar(100),
  PRIMARY KEY(codigo)
);


CREATE TABLE realiza(
  alumno int,
  practica int,
  fecha date,
  nota float,
  PRIMARY KEY(alumno,practica),
  CONSTRAINT uk1 UNIQUE KEY (practica),
  # claves ajenas
  CONSTRAINT fkRealizaPractica FOREIGN KEY (practica) REFERENCES practica(codigo) 
    ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT  fkRealizaAlumno FOREIGN KEY (alumno) REFERENCES alumno(matricula)
    ON DELETE RESTRICT ON UPDATE RESTRICT
);

INSERT INTO alumno 
  (matricula, nombre, grupo) VALUES 
  (1, 'ANA', 'GRUPO A'),
  (2, 'JOSE','GRUPO A'),
  (4, 'LUISA','GRUPO B'),
  (8,'FERNANDO', 'GRUPO C');

INSERT INTO practica 
  (codigo, titulo, dificultad) VALUES 
  (1, 'IMPLEMENTACION ESQUEMA 1_1', 'BASICA'),
  (2, 'IMPLEMENTACION ESQUEMA 1_N', 'BASICA'),
  (3, 'IMPLEMENTACION ESQUEMA N_N', 'AVANZADO');

INSERT INTO realiza 
  (alumno, practica, fecha, nota) VALUES 
  (1, 1, '2024-1-22', 8),
  (1,2,'2024-01-23',9),
  (1,3,'2024-01-24',10);
  -- (2,1,'2024-01-25',6); -- este registro no entra por el indexado sin duplicado (1-n)