﻿-- gestion de la base de datos
DROP DATABASE IF EXISTS practicas;
CREATE DATABASE practicas;
USE practicas;

-- TABLA ALUMNOS

CREATE TABLE alumno(
  matricula int,
  nombre varchar(100),
  grupo varchar(100),
  PRIMARY KEY(matricula)
);

CREATE TABLE practica(
  codigo int,
  titulo varchar(100),
  dificultad varchar(100),
  alumno int,
  nota float,
  fecha date,
  PRIMARY KEY(codigo),
  CONSTRAINT fkPracticaAlumno FOREIGN KEY (alumno) REFERENCES alumno (matricula)
    ON DELETE RESTRICT on UPDATE RESTRICT
  
);

INSERT INTO alumno 
  (matricula, nombre, grupo) VALUES 
  (1, 'ANA', 'GRUPO A'),
  (2, 'JOSE','GRUPO A'),
  (4, 'LUISA','GRUPO B'),
  (8,'FERNANDO', 'GRUPO C');

INSERT INTO practica 
  (codigo, titulo, dificultad,alumno,nota,fecha) VALUES 
  (1, 'IMPLEMENTACION ESQUEMA 1_1', 'BASICA',1,8,'2024-01-22'),
  (2, 'IMPLEMENTACION ESQUEMA 1_N', 'BASICA',1,9,'2024-01-23'),
  (3, 'IMPLEMENTACION ESQUEMA N_N', 'AVANZADO',1,10,'2024-01-24');



